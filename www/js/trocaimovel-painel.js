function carregar_dados_usuario() {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/carregar.dados.usuario.php', {
    token: token
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      if (codigo == 100) {
        carregar_select_estado(null, resposta[0].id_estado, carregar_select_cidade(null, resposta[0].id_estado, resposta[0].id_cidade));
        var nascimento = resposta[0].nascimento;
        if(nascimento == '00/00/0000') nascimento = '';
        $('#nome').val(resposta[0].nome);
        $('#sobrenome').val(resposta[0].sobrenome);
        $('#nascimento').val(nascimento);
        $('#sexo').val(resposta[0].sexo);
        $('#cpf').val(resposta[0].cpf);
        $('#rg').val(resposta[0].rg);
        $('#email-cadastro').val(resposta[0].email_cadastro);
        $('#email-contato').val(resposta[0].email_contato);
        $('#telefone').val(resposta[0].telefone);
        $('#exibir-telefone').val(resposta[0].exibir_telefone);
        $('#alert-loading').hide();
        $('#formulario-editar-dados').show();
      }
      else location.href = 'home.html';
    }
    else location.href = 'home.html';
  });
}

function editar_dados_usuario() {
  var token = localStorage.getItem('token');
  var nome = $('#nome').val();
  var sobrenome = $('#sobrenome').val();
  var nascimento = $('#nascimento').val();
  var sexo = $('#sexo').val();
  var idEstado = $('#select-estado').val();
  var idCidade = $('#select-cidade').val();
  var cpf = $('#cpf').val();
  var rg = $('#rg').val();
  var emailContato = $('#email-contato').val();
  var telefone = $('#telefone').val();
  var exibirTelefone = $('#exibir-telefone').val();
  
  $('.btn-submit').attr('disabled', true);
  $('.btn-submit').html('<i class="fa fa-refresh fa-spin fa-fw"></i> Carregando...');

  $.post( 'http://trocaimovel.com.br/api/editar.dados.usuario.php', {
    token: token,
    nome: nome,
    sobrenome: sobrenome,
    nascimento: nascimento,
    sexo: sexo,
    idEstado: idEstado,
    idCidade: idCidade,
    cpf: cpf,
    rg: rg,
    emailContato: emailContato,
    telefone: telefone,
    exibirTelefone: exibirTelefone
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      var alerta = resposta[0].alerta;

      $('.btn-submit').attr('disabled', false);
      $('.btn-submit').html('<i class="fa fa-check fa-fw" aria-hidden="true"></i> Alterar Dados');

      if (codigo == 100) open_alert_modal(alerta, "close_modal();");
      else open_alert_modal(alerta, "close_modal();");
    }
    else location.href = 'home.html';
  });
}

function editar_senha_usuario() {
  var token = localStorage.getItem('token');
  var senhaAtual = $('#senha-atual').val();
  var novaSenha = $('#nova-senha').val();
  var repetirSenha = $('#repetir-senha').val();
  
  $('.btn-submit').attr('disabled', true);
  $('.btn-submit').html('<i class="fa fa-refresh fa-spin fa-fw"></i> Carregando...');

  $.post( 'http://trocaimovel.com.br/api/editar.senha.usuario.php', {
    token: token,
    senhaAtual: senhaAtual,
    novaSenha: novaSenha,
    repetirSenha: repetirSenha
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      var alerta = resposta[0].alerta;

      $('.btn-submit').attr('disabled', false);
      $('.btn-submit').html('<i class="fa fa-check fa-fw" aria-hidden="true"></i> Alterar Dados');

      if (codigo == 100) {
        $('#senha-atual').val('');
        $('#nova-senha').val('');
        $('#repetir-senha').val('');
        open_alert_modal(alerta, "open_page('painel.menu.html');");
      }
      else open_alert_modal(alerta, "close_modal();");
    }
    else open_page('login.html');
  });
}

/* -------------------------------------------------------------------------------------------------------- */

function carregar_dados_editar_anuncio(idAnuncio) {
  $.post( 'http://trocaimovel.com.br/api/carregar.dados.anuncio.php', {
    idAnuncio: idAnuncio
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      if (codigo == 100) {
        carregar_select_categoria(resposta[0].id_categoria_imovel);
        carregar_select_proposta(resposta[0].id_proposta_imovel);
        carregar_select_estado(null, resposta[0].id_estado, carregar_select_cidade(null, resposta[0].id_estado, resposta[0].id_cidade, carregar_select_bairro(null, resposta[0].id_cidade, resposta[0].id_bairro)));
        
        var nQuartos = resposta[0].numero_quartos;
        var nSuites = resposta[0].numero_suites;
        var nBanheiros = resposta[0].numero_banheiros;
        var nVagas = resposta[0].numero_vagas;
        var metragem = resposta[0].metragem;
        var valorImovel = resposta[0].valor_imovel;
        var valorCondominio = resposta[0].valor_condominio;
        var valorIPTU = resposta[0].valor_iptu;
        
        if (nQuartos == 0) nQuartos = '';
        if (nSuites == 0) nSuites = '';
        if (nBanheiros == 0) nBanheiros = '';
        if (nVagas == 0) nVagas = '';
        if (metragem == 0) metragem = '';
        
        if (valorImovel == 0.00) valorImovel = '';
        else valorImovel = converter_float_real(valorImovel);
        
        if (valorCondominio == 0.00) valorCondominio = '';
        else valorCondominio = converter_float_real(valorCondominio);
        
        if (valorIPTU == 0.00) valorIPTU = '';
        else valorIPTU = converter_float_real(valorIPTU);        

        $('#titulo').val(resposta[0].titulo);
        $('#descricao').val(resposta[0].descricao);        
        $('#n-quartos').val(nQuartos);
        $('#n-suites').val(nSuites);
        $('#n-banheiros').val(nBanheiros);
        $('#n-vagas').val(nVagas);
        $('#metragem').val(metragem);
        $('#valor-imovel').val(valorImovel);
        $('#valor-condominio').val(valorCondominio);
        $('#valor-iptu').val(valorIPTU);
        //if(resposta[0].youtube.length > 0) $('#video-youtube').val('https://www.youtube.com/watch?v='+resposta[0].youtube);
      }
      else open_page('login.html');
    }
    else open_page('login.html');
  });
}

function carregar_grid_anuncio() {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/carregar.grid.anuncios.php', {
    token: token
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      $.each(resposta, function () {
        var template = $('#template').html();
        var id = this['id_anuncio'];
        template = template.replace(/{{id}}/g, id);
        template = template.replace(/{{titulo}}/g, this['titulo']);
        $('#list-menu').append(template);
      });
      $('#list-menu').show();
    }
    else $('#alerta').show();
  });
}

function editar_anuncio(idAnuncio) {
  localStorage.setItem('id-anuncio', idAnuncio);
  open_page('painel.editar.anuncio.html')
}

function confirmar_remocao_anuncio(idAnuncio) {
  open_confirm_modal('Deseja remover este anúncio?', 'remover_anuncio('+idAnuncio+')');
}

function remover_anuncio(idAnuncio) {
  close_modal();
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/remover.anuncio.php', {
    token: token,
    idAnuncio: idAnuncio
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      var alerta = resposta[0].alerta;
      if (codigo == 100){
        $('#item-'+idAnuncio).remove();
        open_alert_modal('Anúncio removido com sucesso!', "close_modal();");
        var totalRegistros = $('#list-menu .item').length;
        if (totalRegistros == 0) {
          $('#list-menu').hide();
          $('#alerta').show();
        }
      }
      else open_alert_modal(alerta, "close_modal();");
    }
    else open_alert_modal('Tente mais tarde novamente.', "close_modal();");
  });
}

function carregar_imagens_editar_anuncio(idAnuncio) {  
  $.post( 'http://trocaimovel.com.br/api/carregar.imagens.anuncio.php', {
    idAnuncio: idAnuncio
  })
  .done(function( data ) {
    close_modal();
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var total = resposta.length;
      var contador = 0;
      $.each(resposta, function () {
        var template = $('#template').html();
        var idImagem = this['id_imagem'];
        contador += 1;
        template = template.replace(/{{idImagem}}/g, idImagem);
        template = template.replace(/{{idAnuncio}}/g, this['id_anuncio']);
        template = template.replace('{{imagem}}', this['imagem']);
        $('#grid-resultado').append(template);
        if (contador < total) $('#imagem-'+idImagem+' img').addClass('m-bottom-20');
      });
      $('#alerta').hide();
      $('#grid-resultado').show();
    }
    else {
      $('#alerta').show();
    }
  });
}

function confirmar_remocao_imagem(idImagem, idAnuncio) {
  open_confirm_modal('Deseja remover esta imagem?', 'remover_imagem_anuncio('+idImagem+', '+idAnuncio+');');
}

function remover_imagem_anuncio(idImagem, idAnuncio) {
  close_modal();
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/remover.imagem.anuncio.php', {
    token: token,
    idImagem: idImagem,
    idAnuncio: idAnuncio
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      var alerta = resposta[0].alerta;
      if (codigo == 100) {
        open_alert_modal(alerta, "close_modal();");
        $('#imagem-'+idImagem).remove();
      }
      else open_page('login.html');
    }
    else open_page('login.html');
  });
}

/*function carregar_grid_anuncios() {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/carregar.grid.anuncios.php', {
    token: token
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      $.each(resposta, function () {
        var template = $('#template-anuncio').html();
        var id = this['id_anuncio'];
        template = template.replace(/{{id}}/g, id);
        template = template.replace(/{{titulo}}/g, this['titulo']);
        $('#list-menu').append(template);
      });
      $('#list-menu').show();
    }
    else $('#alerta').show();
  });
}*/


/* -------------------------------------------------------------------------------------------------------- */

function carregar_anuncios_recomendados() {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/carregar.anuncios.recomendados.php', {
    token: token
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    console.log(resposta);
    if (resposta != null) {
      $.each(resposta, function () {
        var template = $('#template').html();
        var titulo = this['categoria'] + ' para ' + this['proposta'];
        var valorImovel = converter_float_real(this['valor_imovel']);
        
        var imagem = '';
        if (this['imagem'].length > 0) imagem = this['imagem'];
        else imagem = 'img/sem-imagem.png';
        
        if (parseInt(this['numero_quartos']) > 0 ) titulo = this['categoria'] + ' com ' + this['numero_quartos'] + ' quartos para ' + this['proposta'];
        template = template.replace(/{{id}}/g, this['id_anuncio']);
        template = template.replace(/{{imagem}}/g, imagem);
        template = template.replace(/{{titulo}}/g, titulo);
        template = template.replace(/{{bairro}}/g, this['bairro']);
        template = template.replace(/{{cidade}}/g, this['cidade']);
        template = template.replace(/{{uf}}/g, this['uf']);
        template = template.replace(/{{descricao}}/g, this['descricao']);
        template = template.replace(/{{nQuartos}}/g, this['numero_quartos']);
        template = template.replace(/{{nSuites}}/g, this['numero_suites']);
        template = template.replace(/{{nBanheiros}}/g, this['numero_banheiros']);
        template = template.replace(/{{nVagas}}/g, this['numero_vagas']);
        template = template.replace(/{{valor}}/g, valorImovel);
        template = template.replace(/{{pontuacao}}/g, this['pontuacao']);
        $('#grid-resultado').append(template);
        if (this['imagem'].length == 0) $('#sem-imagem-'+idAnuncio).show();
      });
      close_modal();
    }
    else{
      close_modal();
    }
  });
}

/* -------------------------------------------------------------------------------------------------------- */

function carregar_grid_interesses () {
  open_loading_modal(1500);
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/carregar.grid.interesses.php', {
    token: token
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      $.each(resposta, function () {
        var template = $('#template').html();
        var id = this['id_interesse'];
        template = template.replace(/{{id}}/g, id);
        template = template.replace(/{{titulo}}/g, this['titulo']);
        $('#list-menu').append(template);
      });
      $('#list-menu').show();
    }
    else $('#alerta').show();
  });
}

function adicionar_interesse () {
  var token = localStorage.getItem('token');
  var titulo = $('#titulo').val();
  var idCategoria = $('#select-categoria').val();
  var idProposta = $('#select-proposta').val();
  var idEstado = $('#select-estado').val();
  var idCidade = $('#select-cidade').val();
  var idBairro = $('#select-bairro').val();
  var nQuartos = $('#n-quartos').val();
  var nSuites = $('#n-suites').val();
  var nBanheiros = $('#n-banheiros').val();
  var nVagas = $('#n-vagas').val();
  var metragemMinima = $('#metragem-minima').val();
  var metragemMaxima = $('#metragem-maxima').val();
  var valorMinimo = $('#valor-minimo').val();
  var valorMaximo = $('#valor-maximo').val();
  
  $('.btn-submit').attr('disabled', true);
  $('.btn-submit').html('<i class="fa fa-refresh fa-spin fa-fw"></i> Carregando...');

  $.post( 'http://trocaimovel.com.br/api/adicionar.interesse.php', {
    token: token,
    titulo: titulo,
    idCategoria: idCategoria,
    idProposta: idProposta,
    idEstado: idEstado,
    idCidade: idCidade,
    idBairro: idBairro,
    nQuartos: nQuartos,
    nSuites: nSuites,
    nBanheiros: nBanheiros,
    nVagas: nVagas,
    metragemMinima: metragemMinima,
    metragemMaxima: metragemMaxima,
    valorMinimo: valorMinimo,
    valorMaximo: valorMaximo
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      var alerta = resposta[0].alerta;

      $('.btn-submit').attr('disabled', false);
      $('.btn-submit').html('<i class="fa fa-plus-square fa-fw" aria-hidden="true"></i> Adicionar Interesse');

      if (codigo == 100) open_alert_modal(alerta, "open_page('painel.gerenciar.interesses.html');");
      else open_alert_modal(alerta, "close_modal();");
    }
    else open_page('login.html');
  });
}

function editar_dados_interesse (idInteresse) {
  var token = localStorage.getItem('token');
  var titulo = $('#titulo').val();
  var idCategoria = $('#select-categoria').val();
  var idProposta = $('#select-proposta').val();
  var idEstado = $('#select-estado').val();
  var idCidade = $('#select-cidade').val();
  var idBairro = $('#select-bairro').val();
  var nQuartos = $('#n-quartos').val();
  var nSuites = $('#n-suites').val();
  var nBanheiros = $('#n-banheiros').val();
  var nVagas = $('#n-vagas').val();
  var metragemMinima = $('#metragem-minima').val();
  var metragemMaxima = $('#metragem-maxima').val();
  var valorMinimo = $('#valor-minimo').val();
  var valorMaximo = $('#valor-maximo').val();
  
  $('.btn-submit').attr('disabled', true);
  $('.btn-submit').html('<i class="fa fa-refresh fa-spin fa-fw"></i> Carregando...');

  $.post( 'http://trocaimovel.com.br/api/editar.dados.interesse.php', {
    token: token,
    idInteresse: idInteresse, 
    titulo: titulo,
    idCategoria: idCategoria,
    idProposta: idProposta,
    idEstado: idEstado,
    idCidade: idCidade,
    idBairro: idBairro,
    nQuartos: nQuartos,
    nSuites: nSuites,
    nBanheiros: nBanheiros,
    nVagas: nVagas,
    metragemMinima: metragemMinima,
    metragemMaxima: metragemMaxima,
    valorMinimo: valorMinimo,
    valorMaximo: valorMaximo
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      var alerta = resposta[0].alerta;

      $('.btn-submit').attr('disabled', false);
      $('.btn-submit').html('<i class="fa fa-plus-square fa-fw" aria-hidden="true"></i> Adicionar Interesse');

      if (codigo == 100) open_alert_modal(alerta, "open_page('painel.gerenciar.interesses.html');");
      else open_alert_modal(alerta, "close_modal();");
    }
    else open_page('login.html');
  });
}

function confirmar_remocao_interesse (idInteresse) {
  open_confirm_modal('Deseja remover este interesse?', 'remover_interesse('+idInteresse+');');
}

function remover_interesse (idInteresse) {
  close_modal();
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/remover.interesse.php', {
    token: token,
    idInteresse: idInteresse
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      var alerta = resposta[0].alerta;
      if (codigo == 100) {
        $('#item-'+idInteresse).remove();
        open_alert_modal('Interesse removido com sucesso!', "close_modal();");
        var totalRegistros = $('#list-menu .item').length;
        if (totalRegistros == 0) {
          $('#list-menu').hide();
          $('#alerta').show();
        }
      }
      else open_alert_modal(alerta, "close_modal();");
    }
    else open_alert_modal('Ocorreu um erro e o interesse não foi removido. Tente mais tarde novamente.', "close_modal();");
  });
}

function carregar_dados_interesse (idInteresse) {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/carregar.dados.interesse.php', {
    token: token,
    idInteresse: idInteresse
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      var codigo = resposta[0].codigo;
      if (codigo == 100) {
        carregar_select_categoria(resposta[0].id_categoria_imovel);
        carregar_select_proposta(resposta[0].id_proposta_imovel);
        carregar_select_estado(null, resposta[0].id_estado, carregar_select_cidade(null, resposta[0].id_estado, resposta[0].id_cidade, carregar_select_bairro(null, resposta[0].id_cidade, resposta[0].id_bairro)));
        var metragemMinima = resposta[0].metragem_minima;
        var metragemMaxima = resposta[0].metragem_maxima;
        var valorMinimo = resposta[0].valor_minimo;
        var valorMaximo = resposta[0].valor_maximo;
        
        if (metragemMinima == 0) metragemMinima = '';
        if (metragemMaxima == 0) metragemMaxima = '';
        if (valorMinimo == 0.00) valorMinimo = '';
        else valorMinimo = converter_float_real(valorMinimo);
        if (valorMaximo == 0.00) valorMaximo = '';
        else valorMaximo = converter_float_real(valorMaximo);
        
        $('#titulo').val(resposta[0].titulo);   
        $('#n-quartos').val(resposta[0].numero_quartos);
        $('#n-suites').val(resposta[0].numero_suites);
        $('#n-banheiros').val(resposta[0].numero_banheiros);
        $('#n-vagas').val(resposta[0].numero_vagas);
        $('#metragem-minima').val(metragemMinima);
        $('#metragem-maxima').val(metragemMaxima);
        $('#valor-minimo').val(valorMinimo);
        $('#valor-maximo').val(valorMaximo);
      }
      else open_page('login.html');
    }
    else open_page('login.html');
  });
}

function editar_interesse (idInteresse) {
  localStorage.setItem('id-interesse', idInteresse);
  open_page('painel.editar.interesse.html');
}

/* -------------------------------------------------------------------------------------------------------- */

function carregar_grid_combinacao() {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/carregar.combinacoes.php', {
    token: token
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      $.each(resposta, function () {
        var template = $('#template').html();
        template = template.replace(/{{idAnuncioUsuario}}/g, pad(this['id_anuncio_usuario'], 5));
        template = template.replace(/{{idAnuncioAnunciante}}/g, pad(this['id_anuncio_anunciante'], 5));
        template = template.replace(/{{idAnunciante}}/g, this['id_anunciante']);
        template = template.replace(/{{anunciante}}/g, this['anunciante']);
        $('#list-menu').append(template);
      });
      $('#alerta').hide();
      $('#list-menu').show();
    }
    else $('#alerta').show();
  });
}

/* -------------------------------------------------------------------------------------------------------- */

function carregar_contador_usuario() {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/carregar.contador.usuario.php', {
    token: token,
  }) 
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) {
      $('#total-anuncios').html(resposta[0].total_anuncios);
      $('#total-novas-mensagens').html(resposta[0].total_novas_mensagens);
      $('#total-interesses').html(resposta[0].total_interesses);
    }
  });
}

$('.input-upload input[type=file]').change(function(){
  $(this).next().find('input').val($(this).val());
});

$(document).ready(function() {
  carregar_contador_usuario();
});