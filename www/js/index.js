var app = {
  initialize: function() {
    this.bindEvents();
  },
  bindEvents: function() {
    document.addEventListener('deviceready', this.onDeviceReady, false);
  },
  onDeviceReady: function() {
    window.plugins.PushbotsPlugin.initialize("5767db6c4a9efa25498b456a", {"android":{"sender_id":"697152046599"}});
    window.plugins.PushbotsPlugin.on("registered", function(pushToken){
      localStorage.setItem('push-token', pushToken);
    });
  },
  receivedEvent: function(id) {
    var parentElement = document.getElementById(id);
    var listeningElement = parentElement.querySelector('.listening');
    var receivedElement = parentElement.querySelector('.received');
  }
};
app.initialize();

function get_mobile_platform() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;
  if (/windows phone/i.test(userAgent)) { return 0; }
  if (/android/i.test(userAgent)) { return 2; }
  if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) { return 1; }
  return 0;
}