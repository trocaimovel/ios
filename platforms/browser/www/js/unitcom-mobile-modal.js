function select_modal (modal) {
  document.location.href = '#top';
  $('body').addClass('stop-scrolling');
  $('#modal-'+modal).show();
}

function close_modal () {
  setTimeout(function(){
    $('body').removeClass('stop-scrolling');
    $('.modal-container').hide();
  }, 100);
}

function open_alert_modal (modalMessage, btnFuncion) {
  document.location.href = '#top';
  $('body').addClass('stop-scrolling');
  $('.modal-content').html(modalMessage);
  $('.btn-ok').attr('onclick', btnFuncion);
  $('#modal-alert').show();
}

function open_confirm_modal (modalMessage, btnYes, btnNo) {
  document.location.href = '#top';
  $('body').addClass('stop-scrolling');
  $('.modal-content').html(modalMessage);
  $('#btn-yes').attr('onclick', btnYes);
  $('#btn-no').attr('onclick', "close_modal();");
  $('#modal-confirm').show();
}

function open_loading_modal (time) {
  select_modal('loading');
  setTimeout(function(){ close_modal(); }, time);
}