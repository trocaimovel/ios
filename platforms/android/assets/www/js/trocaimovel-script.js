function autenticar_usuario() {
  var email = $('#login-email').val();
  var senha = $('#login-senha').val();
  var pushToken = localStorage.getItem('push-token');
  var plataformaMobile = get_mobile_platform();
  $('.btn-submit').attr('disabled', true);
  $('.btn-submit').html('<i class="fa fa-refresh fa-spin fa-fw"></i> Carregando...');

  $.post( 'http://trocaimovel.com.br/api/autenticar.usuario.php', {
    email: email,
    senha: senha,
    pushToken: pushToken,
    plataformaMobile: plataformaMobile
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    var codigo = resposta[0].codigo;
    var alerta = resposta[0].alerta;
    var token = resposta[0].token;
    var usuario = resposta[0].usuario;
    var idUsuario = resposta[0].id_usuario;

    $('.btn-submit').attr('disabled', false);
    $('.btn-submit').html('Acessar');

    if (codigo == 100) {
      localStorage.setItem('token', token);
      localStorage.setItem('nome-usuario', usuario);
      localStorage.setItem('id-usuario', idUsuario);
      open_page('painel.menu.html');
    }
    else if(codigo == 300){
      open_alert_modal('Usuário inexistente ou senha inválida.', 'close_modal();');
      $('#login-senha').val('');
    }
    else {
      open_alert_modal(alerta, 'close_modal();');
      $('#login-senha').val('');
    }
  });
}

function autenticar_facebook() {
  CordovaFacebook.login({
    permissions: ['email', 'public_profile'],
    onSuccess: function(result) {
      CordovaFacebook.graphRequest({
        path: '/me',
        params: { fields: 'id,first_name,last_name,gender,email' },
        onSuccess: function (userData) {
          $.post( 'http://trocaimovel.com.br/api/autenticar.facebook.php', {response: userData}).done(function(data) {
          var resposta = $.parseJSON(data);
          var codigo = resposta[0].codigo;
          var alerta = resposta[0].alerta;
          var token = resposta[0].token;
          var usuario = resposta[0].usuario;
          var idUsuario = resposta[0].id_usuario;
            if (codigo == 100) {
              localStorage.setItem('token', token);
              localStorage.setItem('nome-usuario', usuario);
              localStorage.setItem('id-usuario', idUsuario);
              open_page('painel.menu.html');
            }
            else {
              open_alert_modal(alerta, 'close_modal();');
              $('#login-senha').val('');
            }
          });
        },
        onFailure: function (result) {
          if (result.error) {
            open_alert_modal('Não foi possível conectar ao Facebook.', 'close_modal();');
          }
        }
      });
      if (result.declined.length > 0) {
        open_alert_modal('Não foi possível conectar ao Facebook.', 'close_modal();');
      }
    },
    onFailure: function(result) {
      if(result.cancelled) {
        open_alert_modal('Não foi possível conectar ao Facebook.', 'close_modal();');
      } else if(result.error) {
        open_alert_modal('Não foi possível conectar ao Facebook.', 'close_modal();');
      }
    }
  });
}

function criar_conta() {
  var nome = $('#nome').val();
  var sobrenome = $('#sobrenome').val();
  var nascimento = '00/00/0000';
  var sexo = 'M';
  var idEstado = $('#formulario-cadastro #select-estado').val();
  var idCidade = $('#formulario-cadastro #select-cidade').val();
  var cpf = '';
  var rg = '';
  var emailCadastro = $('#email-cadastro').val();
  var emailContato = $('#email-cadastro').val();
  var telefone = $('#telefone').val();
  var exibirTelefone = $('#exibir-telefone').val();
  var senha = $('#senha').val();
  var repetirSenha = $('#repetir-senha').val();
  var usuario = nome+' '+sobrenome;
  
  $('.btn-submit').attr('disabled', true);
  $('.btn-submit').html('<i class="fa fa-refresh fa-spin fa-fw"></i> Carregando...');

  $.post( 'http://trocaimovel.com.br/api/criar.conta.php', {
    nome: nome,
    sobrenome: sobrenome,
    nascimento: nascimento,
    sexo: sexo,
    idEstado: idEstado,
    idCidade: idCidade,
    cpf: cpf,
    rg: rg,
    emailCadastro: emailCadastro, 
    emailContato: emailContato,
    telefone: telefone,
    exibirTelefone: exibirTelefone,
    senha: senha,
    repetirSenha: repetirSenha
  })
  .done(function(data) {
    var resposta = $.parseJSON(data);
    var codigo = resposta[0].codigo;
    var alerta = resposta[0].alerta;
    var idUsuario = resposta[0].id_usuario;

    $('.btn-submit').attr('disabled', false);
    $('.btn-submit').html('<i class="fa fa-magic fa-fw"></i> Criar Conta');

    if (codigo == 100) {
      localStorage.setItem('token', resposta[0].token);
      localStorage.setItem('nome-usuario', usuario);
      localStorage.setItem('id-usuario', idUsuario);
      open_alert_modal('Sua conta no Troca Imóvel foi criada com sucesso, deseja acessá-la agora?', 'acessar_conta();');
    }
    else open_alert_modal(alerta, 'close_modal();');
  });
}

function enviar_email_senha() {
  var emailCadastro = $('#senha-email').val();
  var cpf = $('#senha-cpf').val();

  $('.btn-submit').attr('disabled', true);
  $('.btn-submit').html('<i class="fa fa-refresh fa-spin fa-fw"></i> Carregando...');

  $.post( 'http://trocaimovel.com.br/api/enviar.email.senha.php', {
    emailCadastro: emailCadastro,
    cpf: cpf
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    var codigo = resposta[0].codigo;
    var alerta = resposta[0].alerta;

    $('.btn-submit').attr('disabled', false);
    $('.btn-submit').html('Recuperar Senha');

    if (codigo == 100) open_alert_modal('Senha enviada com sucesso!.', "open_page('login.html');");
    else open_alert_modal(alerta, 'close_modal();');
  });
}

function acessar_conta(){
  open_page('painel.menu.html');
}

function carregar_select_estado(formulario, idEstadoAtual, funcao) {
  if (formulario == undefined) formulario = '';
  $.get( 'http://trocaimovel.com.br/api/carregar.select.estado.php').done(function( data ) {
    $(formulario+'#select-estado').html('<option value="0">selecione um estado</option>');
    var resposta = $.parseJSON(data);
    if (resposta != undefined) {
      $.each(resposta, function(){
        var idEstado = this[0];
        var estado = this[1];
        $(formulario+'#select-estado').append('<option value="'+idEstado+'">'+estado+'</option>');
      });
      if (idEstadoAtual != undefined) $(formulario+'#select-estado').val(idEstadoAtual);
    }
    else $(formulario+'#select-estado').html('<option value="0">erro ao carregar</option>');    
    if (typeof(funcao) == "function") funcao.call();
  });
}

function carregar_select_cidade(formulario, idEstado, idCidadeAtual, funcao) {
  if (formulario == undefined) formulario = '';
  $(formulario+'#select-cidade').attr('disabled', true);
  $(formulario+'#select-cidade').html('<option value="0">carregando...</option>');
  $.get( 'http://trocaimovel.com.br/api/carregar.select.cidade.php', {
    idEstado: idEstado
  })
  .done(function(data) {
    $(formulario+'#select-cidade').html('<option value="0">selecione uma cidade</option>');
    var resposta = $.parseJSON(data);
    if (resposta != undefined) {
      $.each(resposta, function(){
        var idCidade = this[0];
        var cidade = this[1];
        $(formulario+'#select-cidade').append('<option value="'+idCidade+'">'+cidade+'</option>');
      });
      if (idCidadeAtual != undefined) $(formulario+'#select-cidade').val(idCidadeAtual);
    }
    else {
      if (idEstado == undefined) $(formulario+'#select-cidade').html('<option value="0">selecione uma cidade</option>');
      else $(formulario+'#select-cidade').html('<option value="0">erro ao carregar</option>');
    }
    $(formulario+'#select-cidade').attr('disabled', false);
    if (typeof(funcao) == "function") funcao.call();
  });
}

function carregar_select_bairro(formulario, idCidade, idBairroAtual) {
  if (formulario == undefined) formulario = '';
  $(formulario+'#select-bairro').attr('disabled', true);
  $(formulario+'#select-bairro').html('<option value="0">carregando...</option>');
  $.get( 'http://trocaimovel.com.br/api/carregar.select.bairro.php', {
    idCidade: idCidade
  })
  .done(function(data) {
    $(formulario+'#select-bairro').html('<option value="0">selecione um bairro</option>');
    var resposta = $.parseJSON(data);
    if (resposta != undefined) {
      $.each(resposta, function(){
        var idBairro = this[0];
        var bairro = this[1];
        $(formulario+'#select-bairro').append('<option value="'+idBairro+'">'+bairro+'</option>');
      });
      if (idBairroAtual != undefined) $(formulario+'#select-bairro').val(idBairroAtual);
    }
    else {
      if (idCidade == undefined) $(formulario+'#select-bairro').html('<option value="0">selecione um bairro</option>');
      else $(formulario+'#select-bairro').html('<option value="0">erro ao carregar</option>');
    }
    $(formulario+'#select-bairro').attr('disabled', false);  
  });
}

function carregar_select_categoria(idCategoriaAtual) {
  $.get( 'http://trocaimovel.com.br/api/carregar.select.categoria.php').done(function(data) {
    $('#select-categoria').html('');
    var resposta = $.parseJSON(data);
    if (resposta != undefined) {
      $.each(resposta, function(){
        var idCategoria = this['id_categoria_imovel'];
        var categoria = this['categoria'];
        $('#select-categoria').append('<option value="'+idCategoria+'">'+categoria+'</option>');
      });
      if (idCategoriaAtual != undefined) $('#select-categoria').val(idCategoriaAtual);
      else $('#select-categoria').val(2);
      $('#select-categoria').attr('disabled', false);
    }
    else $('#select-categoria').html('<option value="0">erro ao carregar</option>'); 
  });
}

function carregar_select_proposta(idPropostaAtual) {
  $.get( 'http://trocaimovel.com.br/api/carregar.select.proposta.php').done(function(data) {
    $('#select-proposta').html('');
    var resposta = $.parseJSON(data);
    if (resposta != undefined) {
      $.each(resposta, function(){
        var idProposta = this['id_proposta_imovel'];
        var proposta = this['proposta'];
        $('#select-proposta').append('<option value="'+idProposta+'">'+proposta+'</option>');
      });
      if (idPropostaAtual != undefined) $('#select-proposta').val(idPropostaAtual);
      $('#select-proposta').attr('disabled', false);
    }
    else $('#select-proposta').html('<option value="0">erro ao carregar</option>'); 
  });
}

function desconectar_usuario() {
  var token = localStorage.getItem('token');
  $.post( 'http://trocaimovel.com.br/api/desconectar.usuario.php', {
    token: token
  })
  .done(function( data ) {
    var resposta = $.parseJSON(data);
    if (resposta != null) open_page('login.html');
  });
}

function verificar_digito(e) {
  if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) return false;
}

function converter_float_real(valor){
  var inteiro = null, decimal = null, c = null, j = null;
  var aux = new Array();
  
  valor = ''+valor;
  c = valor.indexOf('.',0);
  
  if (c > 0) {
    inteiro = valor.substring(0,c);
    decimal = valor.substring(c+1,valor.length);
    if(decimal.length === 1) decimal += '0';
  }
  else inteiro = valor;

  for (j = inteiro.length, c = 0; j > 0; j-=3, c++) aux[c]=inteiro.substring(j-3,j);

  inteiro = '';
  for (c = aux.length-1; c >= 0; c--) inteiro += aux[c]+'.';
   
  inteiro = inteiro.substring(0,inteiro.length-1);
   
  decimal = parseInt(decimal);
  if (isNaN(decimal)) decimal = '00';
  else {
     decimal = ''+decimal;
     if (decimal.length === 1) decimal = "0"+decimal;
  }
  valor = inteiro+','+decimal;
  return valor;
}

function pad(str, max) {
  str = str.toString();
  return str.length < max ? pad("0" + str, max) : str;
}